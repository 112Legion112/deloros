<?php
namespace frontend\models;

use common\models\extended\Members as BaseMembers;

class Members extends BaseMembers{

    public function getFullName(){
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->patronymic;
    }

    public function getCompanySite(){
        return $this->company->site;
    }
}