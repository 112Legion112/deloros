<?php
namespace frontend\models;

use common\models\extended\Page as BasePage;

class Page extends BasePage{

    static public function getText($title){
        return static::findOne(['title' => $title])->text;
    }

}