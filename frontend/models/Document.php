<?php
namespace frontend\models;

use common\models\extended\Document as BaseDocument;
use nemmo\attachments\models\File;

class Document extends BaseDocument{

    public function getDocumentUrl(){
        /* @var $file \nemmo\attachments\models\File */
        $file =  $this->files[0];
        return $file->getUrl();
    }
}