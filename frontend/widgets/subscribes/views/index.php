<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="box newseller">
    <h3>Подписаться на новости</h3>
    <p>Получайте важные новости Фонда и обновления информации о программах поддержки промышленности.<br> Укажите свой
        электронный адрес ниже:</p>

    <?php $form = ActiveForm::begin([
        'action' => Url::to('/subscribe'),
        'options' => ['onsubmit' => "return false;"]
    ]); ?>
    <div class="row successSubscribe text-center">
        <div class="col-sm-7">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Введите ваш эл.адрес'])->label(false) ?>
        </div>
        <div class="col-sm-5">
            <?= Html::submitButton('Подписаться', ['class' => 'bttn-light', 'data-subscribe' => '']) ?>
        </div>
    </div>
    <span class="text-center errorSubscribe"></span>

    <?php ActiveForm::end(); ?>

</div>