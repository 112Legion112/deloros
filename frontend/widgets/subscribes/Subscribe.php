<?php
namespace frontend\widgets\subscribes;

use common\models\Subscribe as SubscribeModel;
use frontend\widgets\subscribes\assets\WidgetAsset;

class Subscribe extends \yii\base\Widget
{

    public function init()
    {
        parent::init();
        WidgetAsset::register($this->getView());
        return true;
    }

    public function run()
    {
        $model = new SubscribeModel();

        return $this->render('index', [
            'model' => $model
        ]);
    }

}
