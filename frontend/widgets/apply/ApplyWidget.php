<?php
namespace frontend\widgets\apply;

use frontend\widgets\apply\assets\WidgetAsset;

class ApplyWidget extends \yii\base\Widget
{
    const DARK = 0;
    const LIGHT = 1;

    public $theme = self::DARK;

    public function init()
    {
        parent::init();
        WidgetAsset::register($this->getView());
        return true;
    }

    public function run()
    {
        return $this->render('index', [
            'theme' => $this->theme
        ]);
    }


}
