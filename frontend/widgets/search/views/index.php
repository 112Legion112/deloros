<?php
use yii\helpers\Html;

?>

<?= Html::beginForm(['/search'], "GET"); ?>
    <div class="input-group">
        <?= Html::input('text', 'q', $query, ["class" => "form-control", "style" => "color:#fff;", 'placeholder' => "Поиск по сайту", 'required' => "required"]); ?>
        <span class="input-group-btn">
<?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-default']); ?>
    </span>
    </div>
<?= Html::endForm(); ?>