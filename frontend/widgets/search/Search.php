<?php
namespace frontend\widgets\search;

class Search extends \yii\base\Widget
{

    public function init()
    {
        parent::init();
        return true;
    }

    public function run()
    {
        return $this->render('index', [
            'query' => \Yii::$app->request->get("q")
        ]);
    }

}
