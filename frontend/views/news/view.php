<?php
/**
 * @var $this yii\web\View;
 * @var $model frontend\models\News;
 */

use slavkovrn\imagecarousel\ImageCarouselWidget;
use yii\helpers\HtmlPurifier;

if(!$title = $model->seo->title) {
    $title = "{$model->name}";
}

if(!$description = $model->seo->description) {
    $description = 'Страница '.$model->name;
}

if(!$keywords = $model->seo->keywords) {
    $keywords = '';
}

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $description,
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords,
]);
$items = [];
foreach($model->getImages() as $image) {
    $items[] = [
        'src' => $image->getUrl('400x300'),
        'alt' => 'ALT TITLE',
    ];
}
//echo '<pre>';
//var_dump($items);die;
//echo '</pre>';
?>

<div class="container news-page fotbot">
    <div class="row">
        <h1 class="width"> <?= HtmlPurifier::process($model->name);?> </h1>
        <div>
            <?= HTMLPurifier::process($model->text);?>
        </div>
    </div>
<div class="row">
    <?= ImageCarouselWidget::widget([
        'id' =>'image-carousel',    // unique id of widget
        'width' => 1000,             // width of widget container
        'height' => 500,            // height of widget container
        'img_width' => 400,         // width of central image
        'img_height' => 300,        // height of central image
        'images' => $items
    ]) ?>
</div>
<div class="row">
    <p>Поделись новостью с друзьями:</p>
<!-- AddToAny BEGIN -->
    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_google_plus"></a>
    <a class="a2a_button_email"></a>
    <a class="a2a_button_whatsapp"></a>
    <a class="a2a_button_vk"></a>
    <a class="a2a_button_copy_link"></a>
    </div>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
</div>
</div>
