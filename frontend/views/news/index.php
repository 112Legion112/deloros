<?php
/**
 * @var $this yii\web\View
 * @var $news frontend\models\News;
 * @var $oneNews frontend\models\News;
*/

use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

$this->title = Yii::t('app','News');
$this->params['breadcrumbs'][] = $this->title;

?>
<div  class="text-center slide_image hidden-xs">
	<img src="/image/main/slidero.jpg">
</div> 
<div class="container">
<?php foreach($news as $oneNews) { ?>
	<div class="row vertival">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<a href="<?=Url::toRoute(['/news/view', 'slug' => $oneNews->slug])?>">
				<img class="img-fluid rounded mb-2 mb-md-0" src="<?= $oneNews->getPreviewImage()->getUrl('200x150') ?>" alt="">
			</a>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12">
			<h3><a class="width" href="<?=Url::toRoute(['/news/view', 'slug' => $oneNews->slug])?>"><?= $oneNews->name ?></a></h3>
			<p><?= HtmlPurifier::process($oneNews->anons); ?></p>
			<div class="large-post-meta">
				<span>
					<i class="fa fa-clock-o"></i>
					<?= date('d.m.Y', strtotime($oneNews->date)) ?>
				</span>
			</div>
		</div>
	</div>
<?php } ?>
</div>