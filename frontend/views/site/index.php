<?php
/**
 * @var $this yii\web\View;
 * @var $news frontend\models\News;
 * @var $slides[] frontend\models\Slider;
 * @var $helpfulLinks[] frontend\models\HelpfulLink;
 * @var $partners[] frontend\models\Partners;
 */
use yii\helpers\HtmlPurifier;
use frontend\models\Page;
use kv4nt\owlcarousel\OwlCarouselWidget;

$this->title = 'Деловая Россия';
?>

<!-- slider start -->

<?php
OwlCarouselWidget::begin([
    'container' => 'div',
    'containerOptions' => [
        'id' => 'container-id-slider',
        'class' => 'hidden-xs'
    ],
    'pluginOptions'    => [
        'autoplay'          => true,
        'autoplayTimeout'   => 4000,
        'items'             => 1,
        'loop'              => true,
        'dots'              => true,
        'rewind'            => false,
        'margin'            => false,
        'pullDrag'          => true,
        'stagePadding'      => true,

               'itemsDesktop'      => [1300, 3],
               'itemsDesktopSmall' => [979, 3]
    ]
]);
?>
<?php if ($slides) foreach($slides as $slide) {
    /* @var $slide frontend\models\Slider */?>
    <div class="item" style="background:url(<?= $slide->getImages()[0]->getUrl() ?>) no-repeat center">
        <div class="container">
            <div class="col-md-6 slide">
                <a href="<?= $slide->url ?>" class="slider-title"><?= $slide->name; ?></a>
                <div class="slider-intro">
                    <p><?= $slide->text ?></p>
                </div>
                <a href="<?= $slide->url ?>" class="bttn atten">Подробнее</a>
            </div>
        </div>
    </div>
<?php } ?>
<?php OwlCarouselWidget::end(); ?>

<!-- slider end -->

<div class="site-index">
    <div class="body-content">
        <div class="container about">
            <?= Page::find()->where(['slug' => 'tekst-na-glavnoy-1'])->one()->text ?>
            <div class="row">
                <div class="col-sm-7">
                    <?= Page::find()->where(['slug' => 'tekst-na-glavnoy-2'])->one()->text ?>
                </div>
                <div class="col-sm-5 last-news">
                    <h3>Последние новости</h3>
                    <?php
                    if ($news) foreach ($news as $oneNews)
                    /* @var $oneNews \frontend\models\News; */
                    {?>
                        <div class="item">
                            <div class="news-date">
                                <span class="month"><?= Yii::$app->formatter->asDate($oneNews->date, 'LLLL'); ?></span>
                                <span class="num"><?= Yii::$app->formatter->asDate($oneNews->date, 'dd'); ?></span>
                            </div>
                            <a href="/news/<?= $oneNews->slug?>" class="title"><?= $oneNews->name?></a>
                            <div class="intro">
                                <p><?=HtmlPurifier::process($oneNews->anons); ?></p>
                            </div>
                            <a href="/news/<?= $oneNews->slug?>" class="readmore">Читать далее</a>
                        </div>
                    <?php } ?>
                </div>
                <div class="text-center">
                    <a href="/news/index" class="bttn">Смотреть все новости</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container box">
        <h2 style="text-align: center;">Полезные ссылки</h2>
        <ul class="row" style="text-align: center;">
        <?php if ($helpfulLinks )
            foreach ($helpfulLinks as $helpfulLink) {
            /* @var $helpfulLink \frontend\models\HelpfulLInk */?>
            <li class="col-md-five col-sm-4 col-xs-6 ">
                <a href="<?= $helpfulLink->url ?>"><img src="<?= $helpfulLink->getImages()[0]->getUrl('250x') ?>" ref="logo"></a>
                <h4><?= $helpfulLink->name ?></h4>
            </li>
        <?php } ?>
        </ul>
    </div>
    <!-- news end -->
    <div class="container box">
        <?= Page::find()->where(['slug' => 'tekst-na-glavnoy-partnery'])->one()->text ?>
        <ul class="row text-center">
        <?php
        if ($partners)
            foreach ($partners as $partner) {
            /* @var $partner frontend\models\Partners; */
            $image = $partner->getImages()[0];
            ?>
            <a href="<?= $partner->url;?>">
                <li class="col-md-five col-sm-4 col-xs-6 ">
                    <div style="height: 150px;">
                        <img src="<?= $image->getUrl('215x')?>" ref="logo" class="main_partner">
                    </div>
                    <h4 class="text_partner"><?= $partner->name?></h4>
                </li>
            </a>
        <?php } ?>
        </ul>
    </div>
</div>
