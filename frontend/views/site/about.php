<?php
/**
 * @var $this \yii\web\View;
 */
$this->title = Yii::t('app','About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div  class="text-center slide_image hidden-xs">
    <img src="/image/main/slidero.jpg">
</div> 
<div class="fotbot">
    <h1 class="text-center">Страница о нас</h1>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="text-center">Контактный номер: <br> <?=  Yii::$app->settings->get('site.phone') ?></h3>
                <h3 class="text-center">Время работы: <br><?=  Yii::$app->settings->get('site.time') ?></h3>
                <h3 class="text-center"><?=  Yii::$app->settings->get('site.address') ?></h3>

            </div>
            <div class="col-sm-6">
                <h3 class="text-center">Мы на карте</h3>
                <div id="map" style="height:300px;width:100%;"></div>
            </div>
        </div>
    </div>
</div>
<script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<?= $this->registerJs("
        ymaps.ready(init);
        function init () {
            ymaps.geocode('" . Yii::$app->settings->get('site.address') . "', { results: 1 }).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0),
                    myMap = new ymaps.Map('map', {
                        center: firstGeoObject.geometry.getCoordinates(),
                        zoom: 12
                    });
    
                myMap.controls.add('zoomControl', { left: 5, top: 5 });
    
                var myPlacemark = new ymaps.Placemark(firstGeoObject.geometry.getCoordinates(), {
                    balloonContent: '" . Yii::$app->settings->get('site.address') . "',
                    hintContent: '" . Yii::$app->settings->get('site.address') . "'
                });
    
                myMap.geoObjects.add(myPlacemark);
            });
        }
    ", yii\web\View::POS_READY); ?>
