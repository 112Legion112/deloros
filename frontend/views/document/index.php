<?php
/**
 * @var $this yii\web\View;
 * @var $documents frontend\models\Document;
 */
$this->title = Yii::t('app','Documents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div  class="text-center slide_image hidden-xs">
	<img src="/image/main/slidero.jpg">
</div> 
<div class="container fotbot">
	<h1 class="my-4">Документы
	</h1>
		<div class="row vertival">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div>
				<?php
				if ($documents)
				    foreach($documents as $document) {
				    /* @var $document frontend\models\Document; */
				    ?>
				    <div class="text-center">
				    	<h3>
					    	<strong><?= $document->name ?>:</strong>
					        <a href="<?= $document->getDocumentUrl()?>">Скачать</a>
				        </h3>
				    </div>
				<?php }?>
				</div>
			</div>
	</div>
</div>
