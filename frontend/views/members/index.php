<?php
/* @var $this yii\web\View; */
/* @var $categories frontend\models\MemberCategory */
/* @var $members frontend\models\Members */

use yii\helpers\Url;

$this->title = Yii::t('app','Members');
$this->params['breadcrumbs'][] = $this->title;
?>
<div  class="text-center slide_image">
	<img src="/image/main/slidero.jpg">
</div> 
<?php foreach($categories as $category) { ?>
	<div class="container size nopad">
		<div class="text-center" >
			<div class="row">
				<h3 class="text-center member_header width"><?= $category->title; ?></h3>
				<?php foreach($members as $member) {
				    /** @var $member \frontend\models\Members */?>
					<?php if($member->category == $category->id) { ?>
						<a href="<?=Url::toRoute(['/members/member/', 'id' => $member->id])?>">
							<div class="col-xs-6 col-sm-4 col-md-3 blog_box text-center rader">
								<div class="member-het">
									<img class="linkmem alignimage" src="<?=$member->getAvatarUrl('150x150')?>">
									<h3 class="membercl linkmem">
										<span><?=$member->getFullName();?></span>
									</h3>
									<p class="linkmem membertext"><?=$member->title;?></p>
								</div>
                                <a target="_blank" href="<?= $member->getCompanySite()?>"><?= $member->getCompanySite()?></a>
							</div>
						</a>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>