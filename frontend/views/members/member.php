<?php
/* @var $this yii\web\View; */
/* @var $categories frontend\models\MemberCategory */
/* @var $member frontend\models\Members */

use yii\helpers\Url;
use yii\helpers\HtmlPurifier;

$this->title = $member->getFullName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $member->getFullName();
?>
<div class="container size">
	<div class="col-md-12 alignbody">
		<div class="col-md-5 col-sm-12 col-xs-12">
			<h2><?=$member->getFullName()?></h2>
			<img src="<?=$member->getAvatarUrl("200x")?>">
			<p><?=$member->title ?></p>
			<p>Предприятие: <?=$member->getCompanyName()?></p>
		    <?php if ($member->company) { ?>
			<img src="<?=$member->company->getLogo("100x")?>">
		    <?php } ?>
	    </div>
		<div class="col-md-7 col-sm-12 col-xs-12 alignt">
			<ul class="nav nav-tabs col-md-12 alignment" role="tablist">
				<li class="active size-tab"><a href="#description" aria-expanded="true" data-toggle="tab">Описание</a></li>
				<li class="size-tab"><a href="#news" aria-expanded="true" data-toggle="tab">Новости</a></li>
				<li class="size-tab"><a href="#prod_and_services" aria-expanded="true" data-toggle="tab">Продукция и сервисы</a></li>
				<li class="size-tab"><a href="#contacts" aria-expanded="true" data-toggle="tab">Контакты</a></li>
			</ul>
		<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="description"><?=$member->text?></div>
		<div role="tabpanel" class="tab-pane" id="news">
		<?php foreach($member->company->news as $oneNews) {
		    /** @var $oneNews \frontend\models\News; */?>
			<div class="col-md-12 col-sm-12 col-xs-12 tabpanelh" style="padding: 10px 0 10px 0">
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <a href="<?=Url::toRoute(['/news/view', 'slug' => $oneNews->slug])?>">
                        <img class="img-fluid rounded mb-2 mb-md-0" src="<?= $oneNews->getPreviewImage()->getUrl('170x170') ?>" alt="">
                    </a>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-12 alignment">
                	<div class="flex-member">
	                    <h3 style="margin-top: 0"><a href="<?=Url::toRoute(['/news/view', 'slug' => $oneNews->slug])?>"><?= $oneNews->name ?></a></h3>
	                    <p><?= HtmlPurifier::process($oneNews->anons); ?></p>
	                    <div class="large-post-meta">
						<span>
							<i class="fa fa-clock-o"></i>
		                    <?= date('d.m.Y', strtotime($oneNews->date)) ?>
						</span>
	                    </div>
                    </div>
                </div>
			</div>
		<?php } ?>
		</div>
		<div role="tabpanel" class="tab-pane" id="prod_and_services">
			<div>
				<h3>Продукция</h3>
				<ul class="atten">
					<?php foreach ($member->company->products as $product) { ?>
						<li><?=$product->name?></li>
					<?php } ?>
				</ul>
				<h3>Сервисы</h3>
				<ul class="atten">
					<?php foreach ($member->company->services as $service) { ?>
						<li><?=$service->name?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="contacts">
			<ul class="atten">
                <?= ($member->phone) ? '<li><h3>Контактный номер :' .  $member->phone . '</h3></li>' : '' ?>
                <?= ($member->company->site) ? '<li class="memlink"><h3>Сайт : <a href="'. $member->company->site .'">'. $member->company->site .'</a></h3></li>' : '' ?>
                <?php if($member->company->address) { ?><div id="map" style="height:300px;width:100%;"></div><?php } ?>
            </ul>
            </div>
		</div>
	    </div>
        </div>
	</div>


</div>
<?php if($member->company->address) { ?>
    <script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <?= $this->registerJs("
        ymaps.ready(init);
        function init () {
            ymaps.geocode('" . $member->company->address . "', { results: 1 }).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0),
                    myMap = new ymaps.Map('map', {
                        center: firstGeoObject.geometry.getCoordinates(),
                        zoom: 12
                    });
    
                myMap.controls.add('zoomControl', { left: 5, top: 5 });
    
                var myPlacemark = new ymaps.Placemark(firstGeoObject.geometry.getCoordinates(), {
                    balloonContent: '" . $member->company->name . "',
                    hintContent: '" . $member->company->address . "'
                });
    
                myMap.geoObjects.add(myPlacemark);
            });
        }
    ", yii\web\View::POS_READY); ?>
<?php } ?>