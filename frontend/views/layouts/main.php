<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 * @var $settings \pheme\settings\components\Settings;
 */

use frontend\models\Page;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\AppAsset;
use frontend\models\Feedback;
use yii\widgets\Breadcrumbs;

$settings = Yii::$app->settings;
$settings->clearCache();
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $settings->get('yandex.metrika');?>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
<header>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <ul class="col-md-12">
                    <li><i class="fa fa-map-marker"></i> <a target="_blank"><?= $settings->get('site.address') ?></a></li>
                    <li><i class="fa fa-envelope"></i> <a><?= $settings->get('site.email') ?></a></li>
                    <li><i class="fa fa-phone"></i> <a><?= $settings->get('site.phone') ?></a></li>
                    <li><a>Курганское региональное отделение</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="sticky">
        <div class="container">
            <div class="row">
                <div class="col-xs-9 col-sm-3 col-md-5">
                    <a <?= Yii::$app->request->url != "/" ? 'href="/"' : '' ?> class="logo">
                        <img src="/image/deloros-logo.png" alt="">
                        <span></span>
                    </a>
                </div>

                <div class="col-sm-9 col-md-7">
                    <button class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <ul class="collapse navbar-collapse col-xs-12" id="menu">
                        <li>
                            <a href="/members">Члены</a>
                        </li>
                       <!--  <li>
                            <a href="/document/index">Документы</a>
                        </li> -->
                        <li>
                            <a href="/news/index">Новости</a>
                        </li>
                        <li>
                            <a href="/site/about">Контакты</a>
                        </li>
                        <li>
                            <a class="limodal" href="#myModal" data-toggle='modal'>Вступить</a>
                        </li>
                    </ul>
                </div>

                <!-- menu end -->

                <!-- HTML-код модального окна -->

                <div id="myModal" class="modal fade">
                  <div class="modal-dialog">
                    <div class="modal-contentt">
                      <!-- Заголовок модального окна -->
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Форма обратной связи</h4>
                      </div>
                      <!-- Основное содержимое модального окна -->
                      <div class="modal-body">
                        <!-- Форма Отправки -->
                        <?php
                        $model = new Feedback();
                        $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'action' => '/site/join',
                                'options' => ['class' => 'form-horizontal'],
                            ]);?>
                        <?= $form->field($model, 'name'); ?>

                        <?= $form->field($model, 'phone'); ?>

                        <?= $form->field($model, 'agreement')->checkbox([]) ?>

                        <?= Html::submitButton('Отправить',['class' => 'btn btn-primary']) ?>
                        <?php ActiveForm::end(); ?>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
    <?php echo Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
    ?>
    </div>
</header>

<?= $content ?>
<footer class="container-fluid">
    <div class="container">
        <div class="row footer-top">
            <div class="col-sm-4">
                <?= Page::getText('Подвал левая колонка'); ?>
            </div>
            <div class="col-sm-3">
                <div class="footer-title">
                </div>
                <ul class="footer-menu">
                    <li><a href="/members">Члены</a></li>
                    <!-- <li><a href="/document/index">Документы</a></li> -->
                    <li><a href="/news/index">Новости</a></li>
                    <li><a href="/site/about">Контакты</a></li>
                    <li><a href="#myModal" data-toggle="modal">Вступить</a></li>
                </ul>
            </div>
            <div class="col-sm-4 col-sm-offset-1">
                <div class="footer-title">
                    КОНТАКТНАЯ ИНФОРМАЦИЯ
                </div>
                <p><?= Page::getText('Подвал контактная информация') ?></p>
                <ul>
                    <li><i class="fa fa-map-marker"></i> <a href=""> <?= $settings->get('site.address') ?> </a></li>
                    <li><i class="fa fa-envelope"></i> <a href=""><?= $settings->get('site.email') ?></a>
                    </li>
                    <li><i class="fa fa-phone"></i> <a href=""></a><?= $settings->get('site.phone')?> </li>
                </ul>
            </div>
        </div>
        <div class="row footer-bottom">
            <div class="col-sm-6">
                <?= "© " . date("Y") . " " . Page::getText('Подвал копирайт') ?>
            </div>
            <div class="col-sm-6 text-right">
            </div>
        </div>
    </div>
</footer>
    <?php $this->endBody() ?>
</body>

<?php $this->endPage() ?>
