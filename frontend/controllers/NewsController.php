<?php
namespace frontend\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\models\News;

class NewsController extends Controller
{
    public function actionIndex()
    {
        $news = News::find()->where(['status' => News::STATUS_PUBLISHED])->orderby(['date' => SORT_DESC, 'id' => SORT_DESC])->all();

        return $this->render('index',[
            'news' => $news,
        ]);
    }

    public function actionView($slug){

        if(!$model = News::findOne(['slug' => $slug])){
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $this->render('view',[
            'model' => $model,
        ]);
    }

}