<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Members;
use frontend\models\MemberCategory;

class MembersController extends Controller
{
    public function actionIndex()
	{
		$categories = MemberCategory::find()->orderBy('id ASC')->all();
		$members = Members::find()->orderBy('category ASC')->all();
		
        return $this->render('index', [
			'categories' => $categories,
			'members' => $members
		]);
    }
	
	public function actionMember($id)
	{
		//$id = Yii::$app->request->get('id');
		$member = Members::findOne((int)$id);
		
		return $this->render('member', [
			'member' => $member
		]);
	}
}