<?php
namespace frontend\controllers;

use frontend\models\Feedback;
use frontend\models\HelpfulLInk;
use frontend\models\News;
use frontend\models\Partners;
use frontend\models\Slider;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['join'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $news = News::find()->where(['status' => News::STATUS_PUBLISHED])->orderBy(['date' => SORT_DESC])->limit(3)->all();
        $slides = Slider::find()->all();
        $partners = Partners::find()->orderBy('id')->all();
        $helpfulLinks = HelpfulLInk::find()->all();

        return $this->render('index',[
            'slides' => $slides,
            'news' => $news,
            'partners' => $partners,
            'helpfulLinks' => $helpfulLinks,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionJoin(){
        $model = new Feedback();
        if ( $model->load(Yii::$app->request->post()) && $model->save() ){
            return $this->redirect(['thanks']);
        }
    }

    public function actionThanks(){
        return $this->render('thanks');
    }
}
