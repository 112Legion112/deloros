<?php
namespace frontend\controllers;

use frontend\models\Document;
use yii\web\Controller;

class DocumentController extends Controller {

    function actionIndex(){

        $documents = Document::find()->where(['is_show' => Document::SHOW_IS_YES])->orderBy('id', SORT_DESC)->all();
        return $this->render('index',[
            'documents' => $documents,
        ]);
    }

}