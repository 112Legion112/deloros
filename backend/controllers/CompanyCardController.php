<?php

namespace backend\controllers;

use backend\models\CompanyProduct;
use backend\models\CompanyService;
use backend\models\Model;
use Yii;
use backend\models\CompanyCard;
use backend\models\search\CompanyCardSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CompanyCardController implements the CRUD actions for CompanyCard model.
 */
class CompanyCardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyCard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyCardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanyCard model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CompanyCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $company = new CompanyCard;
        $services = [new CompanyService];
        $products = [new CompanyProduct];

        if ($company->load(Yii::$app->request->post())) {

            $services = Model::createMultiple(CompanyService::class);
            Model::loadMultiple($services, Yii::$app->request->post());
            $products = Model::createMultiple(CompanyProduct::class);
            Model::loadMultiple($products, Yii::$app->request->post());


            //ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->formatter = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($services),
                    ActiveForm::validate($company),
                    ActiveForm::validateMultiple($products)
                );
            }

            // validate all models
            if ($company->validate() && Model::validateMultiple($services) && Model::validateMultiple($products)) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $company->save(false)){
                        foreach($services as $service) {
                            /** @var $service \backend\models\CompanyService */
                            $service->company_id = $company->id;
                            if (! ($flag = $service->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach($products as $product){
                            /** @var $product \backend\models\CompanyProduct */
                            $product->company_id = $company->id;
                            if(! ($flag = $product->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id'=>$company->id]);
                    }
                }catch (\Exception $e){
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'company' => $company,
            'services' => (empty($services)) ? [new CompanyService] : $services,
            'products' => (empty($products)) ? [new CompanyProduct] : $products,
        ]);
    }

    /**
     * Updates an existing CompanyCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $company = $this->findModel($id);
        $services = $company->services;
        $products = $company->products;

        if ($company->load(Yii::$app->request->post())){

            $oldIDsOfServices = ArrayHelper::map($services, 'id','id');
            $services = Model::createMultiple(CompanyService::class, $services);
            Model::loadMultiple($services, Yii::$app->request->post());

            $oldIdsOfProducts = ArrayHelper::map($products, 'id', 'id');
            $products = Model::createMultiple(CompanyProduct::class, $products);
            Model::loadMultiple($products, Yii::$app->request->post());

            $deletedIDsOfServices = array_diff($oldIDsOfServices, array_filter(ArrayHelper::map($services, 'id', 'id')));
            $deletedIDsOfProducts = array_diff($oldIdsOfProducts, array_filter(ArrayHelper::map($products, 'id', 'id')));

            //ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($products),
                    ActiveForm::validateMultiple($services),
                    ActiveForm::validate($company)
                );
            }

            //validate all models
            if($company->validate() && Model::validateMultiple($services) && Model::validateMultiple($products)){
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $company->save(false)) {
                        if(! empty($deletedIDsOfServices)){
                            CompanyService::deleteAll(['id'=>$deletedIDsOfServices]);
                        }
                        foreach($services as $service){
                            /** @var $service \backend\models\CompanyService; */
                            $service->company_id = $company->id;
                            if (! ($flag = $service->save(false))){
                                $transaction->rollBack();
                                break;
                            }
                        }
                        if(! empty($deletedIDsOfProducts)){
                            CompanyProduct::deleteAll(['id'=>$deletedIDsOfProducts]);
                        }
                        foreach ($products as $product){
                            /** @var $product \backend\models\CompanyProduct */
                            $product->company_id = $company->id;
                            if(! ($flag = $product->save(false))){
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id'=>$company->id]);
                    }
                }catch (\Exception $e){
                    $transaction->rollBack();
                }
            }
        }


        return $this->render('update', [
            'company' => $company,
            'services' => (empty($services)) ? [new CompanyService] : $services,
            'products' => (empty($products)) ? [new CompanyProduct] : $products,
        ]);
    }

    /**
     * Deletes an existing CompanyCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);

        $model->deleteChildrenModels( $model->news );
        $model->deleteChildrenModels( $model->products );
        $model->deleteChildrenModels( $model->services );
        $model->deleteChildrenModels( $model->members );

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyCard::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
