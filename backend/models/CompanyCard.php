<?php
namespace backend\models;

use common\models\extended\CompanyCard as BaseCompanyCard;

class CompanyCard extends BaseCompanyCard{

    public function deleteChildrenModels($models){
        foreach($models as $model) {
            /** @var $model \yii\db\ActiveRecord */
            $model->delete();
        }
    }

}