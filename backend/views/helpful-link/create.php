<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\HelpfulLink */

$this->title = Yii::t('app', 'Create Helpful Link');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Helpful Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="helpful-link-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
