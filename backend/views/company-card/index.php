<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CompanyCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Company Cards');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-card-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Company Card'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'label' => Yii::t('app', 'Photo'),
                'format' => 'raw',
                'value' => function($model) {
                    /* @var $model backend\models\News */
                    $imageUlr = $model->getImages()[0]->getUrl("100x");
                    if ($imageUlr)
                        return '<img src="' . $imageUlr . '">';
                }
            ],
            'name',
//            'description',
            'phone',
            'email:email',
            //'site',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
