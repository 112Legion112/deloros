<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View;
 * @var $company backend\models\CompanyCard;
 * @var $services backend\models\CompanyService;
 * @var $products backend\models\CompanyProduct;
 */


$this->title = Yii::t('app', 'Update Company Card: {nameAttribute}', [
    'nameAttribute' => $company->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $company->name, 'url' => ['view', 'id' => $company->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="company-card-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'company' => $company,
        'services' => $services,
        'products' => $products,
    ]) ?>

</div>
