<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\MaskedInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $company backend\models\CompanyCard
 * @var $form yii\widgets\ActiveForm
 * @var $services backend\models\CompanyService
 * @var $products backend\models\CompanyProduct
 */


?>

<div class="company-card-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options' => ['enctype' => 'multipart/form-data']]); ?>

    <?=\dvizh\gallery\widgets\Gallery::widget(
        [
            'model' => $company,
//            'previewSize' => '50x50',
            'fileInputPluginLoading' => true,
            'fileInputPluginOptions' => []
        ]
    ); ?>

    <?= $form->field($company, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($company, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($company, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($company, 'phone')->widget( MaskedInput::class, ['mask' => '+7(999)999-99-99'])?>

    <?= $form->field($company, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($company, 'site')->textInput(['maxlength' => true]) ?>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> <?= Yii::t('app', 'Services') ?></h4></div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper_service', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items-service', // required: css class selector
                'widgetItem' => '.item-service', // required: css class
                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                'min' => 0, // 0 or 1 (default 1)
                'insertButton' => '.add-item-service', // css class
                'deleteButton' => '.remove-item-service', // css class
                'model' => $services[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'name',
                ],
            ]); ?>

            <div class="container-items-service"><!-- widgetContainer -->
                <?php foreach ($services as $i => $service): ?>
                    <div class="item-service panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left"><?= Yii::t('app', 'Service') ?></h3>
                            <div class="pull-right">
                                <button type="button" class="remove-item-service btn btn-danger btn-xs">Sub</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (! $service->isNewRecord) {
                                echo Html::activeHiddenInput($service, "[{$i}]id");
                            }
                            ?>
                            <?= $form->field($service, "[{$i}]name")->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <button type="button" class="add-item-service btn btn-success btn-xs">Add</button>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> <?= Yii::t('app', 'Products') ?></h4></div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper_product', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items-product', // required: css class selector
                'widgetItem' => '.item-product', // required: css class
                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                'min' => 0, // 0 or 1 (default 1)
                'insertButton' => '.add-item-product', // css class
                'deleteButton' => '.remove-item-product', // css class
                'model' => $products[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'name',
                ],
            ]); ?>

            <div class="container-items-product"><!-- widgetContainer -->
                <?php foreach ($products as $i => $product): ?>
                    <div class="item-product panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left"><?= Yii::t('app', 'Product') ?></h3>
                            <div class="pull-right">
                                <button type="button" class="remove-item-product btn btn-danger btn-xs">Sub</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (! $product->isNewRecord) {
                                echo Html::activeHiddenInput($product, "[{$i}]id");
                            }
                            ?>
                            <?= $form->field($product, "[{$i}]name")->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <button type="button" class="add-item-product btn btn-success btn-xs">Add</button>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>


        <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
