<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use nemmo\attachments\components\AttachmentsTable;

/* @var $this yii\web\View */
/* @var $model backend\models\Document */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= AttachmentsTable::widget(['model' => $model]) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'is_show',
                'value' => function($model){
                    /* @var $model backend\models\Document; */
                    if ($model->is_show) {
                        $translate = $model::getShowOptions();
                        return $translate[$model->is_show];
                    }
                }
            ],
            'slug',
        ],
    ]) ?>

</div>
