<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CompanyProduct */

$this->title = Yii::t('app', 'Create Company Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
