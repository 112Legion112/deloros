<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CompanyService */

$this->title = Yii::t('app', 'Create Company Service');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-service-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
