<?php

use backend\models\MemberCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\models\Members;
use backend\models\CompanyCard;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MembersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Members');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="members-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Members'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'category',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'category',
                    ArrayHelper::map(MemberCategory::find()->all(),'id', 'title'),
                    ['prompt' => '-']
                ),
                'value' => function($model){

                    if($model->category) {
                        $translate = ArrayHelper::map(MemberCategory::find()->all(),'id', 'title');
                        return $translate[$model->category];
                    }
                },
            ],
            [
                'label' => Yii::t('app', 'Photo'),
                'format' => 'raw',
                'value' => function($model){
                    /* @var $model backend\models\Members; */
                    $imageUlr = $model->getAvatarUrl("150x");
                    if ($imageUlr)
                        return '<img src="' .  $imageUlr . '">';
                }
            ],

            'last_name',
//            'first_name',
//            'patronymic',
            //'text',
            //'title',
            [
                'label' => Yii::t('app','Company name'),
                'attribute' => 'company_card_id',
                'value' => function($model) {
                    if ($model->company_card_id){
                        return CompanyCard::findONe(['id' => $model->company_card_id])->name;
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
