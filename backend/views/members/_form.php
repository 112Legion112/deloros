<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Members;
use common\models\CompanyCard;
use backend\models\MemberCategory;
use \dvizh\gallery\widgets\Gallery;

/* @var $this yii\web\View */
/* @var $model backend\models\Members */
/* @var $form yii\widgets\ActiveForm */
$categories = ArrayHelper::map(MemberCategory::find()->all(), 'id', 'title');
$companies = ArrayHelper::map(CompanyCard::find()->all(),'id','name');
?>

<div class="members-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'category')->dropDownList($categories) ?>

    <?= Gallery::widget(
        [
            'model' => $model,
            'previewSize' => '50x50',
            'fileInputPluginLoading' => true,
            'fileInputPluginOptions' => []
        ]
    ); ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_card_id')->dropDownList($companies,['prompt'=>Yii::t('app', 'Company name')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
