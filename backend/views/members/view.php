<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Members */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="members-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => Yii::t('app','Photo'),
                'format' => 'raw',
                'value' => function($model){
                    /* @var $model backend\models\Members */
                    $urlImage = $model->getAvatarUrl('200x200');
                    return '<img src=' . $urlImage .'>';
                }
            ],
            'id',
            'category',
            'last_name',
            'first_name',
            'patronymic',
            'text',
            'title',
            [
                'label' => Yii::t('app',"Company name"),
                'attribute' => 'company_card_id',
                'value' => function($model){
                    /* @var $model backend\models\Members */
                    if ($model->getCompanyName())
                        return $model->getCompanyName();
                }
            ],
        ],
    ]) ?>

</div>
