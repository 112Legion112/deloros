<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header" id="my-color-top">

    <?= Html::a('<span class="logo-mini" id="my-color-top">▄▀▄</span><span class="logo-lg" id="my-color-top">' . '<i class="fa fa-sitemap"></i>' . ' Опции панели' . '</span>', Yii::$app->homeUrl, ['class' => 'logo', 'id' => 'my-color-top']) ?>

    <nav class="navbar navbar-static-top" id="my-color-top" role="navigation">

        <a href="#" class="sidebar-toggle" id="my-push-menu" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li>
                    <a href="/" class="btn" style="border-radius: 30px 3px;">
                        На главную сайта
                    </a>
                </li>
                <li>
                    <?= Html::a('ВЫХОД',
                        ['/user/security/logout'],
                        ['data-method' => 'post', 'class' => 'btn', 'style'=>'border-radius: 30px 3px;']
                    ) ?>
                </li>
            </ul>
        </div>
    </nav>
</header>
