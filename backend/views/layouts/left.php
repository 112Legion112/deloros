<aside class="main-sidebar" id="main-my-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'id'=>'my-sidebar-menu', 'data-widget' => 'tree'],
                'items' => [
                    [
                        'label' => 'Члены',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Члены', 'url' => ['/members']],
                            ['label' => 'Категории членов', 'url' => ['/member-category']]
                        ],
                    ],

                    [
                        'label' => 'Компании',
                        'icon' => 'users',
                        'url' => '#',
                        'options' => ['id'=>'my-sidebar-menu-ul'],
                        'items' => [
                            ['label' => 'Компании', 'url' => ['/company-card']],
                            ['label' => 'Продукция', 'url' => ['/company-product']],
                            ['label' => 'Сервисы', 'url' => ['/company-service']],
                        ],
                    ],
                    ['label' => 'Новости', 'icon' => 'bullhorn', 'url' => ['/news']],
                    ['label' => 'Настройки сайта', 'icon' => 'child', 'url' => ['/settings/default']],
                    ['label' => 'Документы', 'icon' => 'gg', 'url' => ['/document']],
                    ['label' => 'Партнеры', 'icon' => 'gg', 'url' => ['/partners']],
                    ['label' => 'Слайдер', 'icon' => 'gg', 'url' => ['/slider']],
                    ['label' => 'Страницы', 'icon' => 'gg', 'url' => ['/page']],
                    ['label' => 'Вступить', 'icon' => 'gg', 'url' => ['/feedback']],
                    ['label' => 'Полезные ссылки', 'icon' => 'gg', 'url' => ['/helpful-link']],
                ],
            ]
        ) ?>
    </section>
</aside>
