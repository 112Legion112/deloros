<?php

use common\models\News;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create News'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'label' => Yii::t('app', 'Photo'),
                'format' => 'raw',
                'value' => function($model) {
                    /* @var $model backend\models\News */
                    $imageUlr = $model->getPreviewImage()->getUrl("100x");
                    if ($imageUlr)
                        return '<img src="' . $imageUlr . '">';
                }
            ],
            'name',
            [

                'label' => Yii::t('app','Company name'),
                'attribute' => 'companyName',
                'value' => function($model) {
                /* @var $model backend\models\News */
                if($model->getCompanyName())
                    return $model->getCompanyName();
                }
            ],
//            'anons',
//            'text',
            [
                'value' => 'date',
                'filter'=> DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
            ],
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    News::getStatusOptions(),
                    ['prompt' => News::getStatusPrompt()]
                ),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->status) {
                        $translate = News::getStatusOptions();
                        return $translate[$model->status];
                    }
                }
            ],
            //'slug',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
