<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\News;
use common\models\CompanyCard;
use vova07\imperavi\Widget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */

$companies = ArrayHelper::map(CompanyCard::find()->all(),'id', 'name');
?>

<div class="news-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?=\dvizh\seo\widgets\SeoForm::widget([
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?=\dvizh\gallery\widgets\Gallery::widget(
        [
            'model' => $model,
            'previewSize' => '50x50',
            'fileInputPluginLoading' => true,
            'fileInputPluginOptions' => []
        ]
    ); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'anons')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(Widget::class, [
        'settings' => [
            'minHeight' => 200,
            'pastePlainText' => true,
            'buttonSource' => true,
            'imageUpload' => Url::to(['/news/image-upload']),
            'plugins' => [
                'imagemanager'
            ],
        ]
    ])?>



    <?= $form->field($model, 'status')->dropDownList(News::getStatusOptions(), ['prompt' => News::getStatusPrompt()]) ?>

    <?= $form->field($model, 'company_card_id')->dropDownList($companies, ['prompt'=>Yii::t('app', 'Company name')]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
