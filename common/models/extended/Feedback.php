<?php
namespace common\models\extended;

use common\models\Feedback as BaseFeedback;

use Yii;

class Feedback extends BaseFeedback
{
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_BEFORE_INSERT,[$this, 'sendMail']);
    }

    public function sendMail($event){
        /** @var $settings \pheme\settings\components\Settings */
        $settings = Yii::$app->settings;
        Yii::$app->mailer->compose('feedback')
            ->setTo($settings->get('site.email'))
            ->setFrom('deloros45.ru@gmail.com')
            ->setSubject(Yii::t('app', 'New feedback'))
            ->send();
    }
}