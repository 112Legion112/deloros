<?php
namespace common\models\extended;

use common\models\News as BaseNews;
use dvizh\gallery\models\Image;

/**
 * @property CompanyCard $companyCard;
 */
class News extends BaseNews
{
    public function getCompanyName()
    {
        if ($this->companyCard)
            return $this->companyCard->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCard()
    {
        return $this->hasOne(CompanyCard::class, ['id' => 'company_card_id']);
    }

    public function getPreviewImage()
    {
        $preview = $this->getImages()[0];

        foreach ($this->getImages() as $image){
            /** @var $image Image */
            if ($image->isMain){
                $preview = $image;
            }

        }
        return $preview;
    }
}