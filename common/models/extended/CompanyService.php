<?php
namespace common\models\extended;

use common\models\CompanyService as BaseCompanyService;

/**
 * @property CompanyCard company
 */
class CompanyService extends BaseCompanyService
{
    public function getCompanyName(){
        if ($this->company)
            return ($this->company->name);
    }

    public function getCompany(){
        return $this->hasOne(CompanyCard::class, ['id' => 'company_id']);
    }
}