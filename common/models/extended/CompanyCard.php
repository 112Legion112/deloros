<?php
namespace common\models\extended;

use common\models\CompanyCard as BaseCompanyCard;
/**
 * @property News $news[]
 * @property CompanyProduct[] $products[]
 * @property CompanyService[] $services[]
 * @property Members[]        $members[]
 */

class CompanyCard extends BaseCompanyCard{

    public function getLogo($size){
        return $this->getImages()[0]->getUrl($size);
    }

    public function getNews(){
        return $this->hasMany(News::class,['company_card_id' => 'id']);
    }

    public function getProducts(){
        return $this->hasMany(CompanyProduct::class,['company_id' => 'id']);
    }

    public function getServices(){
        return $this->hasMany(CompanyService::class,['company_id' => 'id']);
    }

    public function getMembers(){
        return $this->hasMany(Members::class,['company_card_id' => 'id']);
    }
}