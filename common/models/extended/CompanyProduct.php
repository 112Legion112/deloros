<?php namespace common\models\extended;

use common\models\CompanyProduct as BaseCompanyProduct;

/**
 * @property CompanyCard $companyCard
 */
class CompanyProduct extends BaseCompanyProduct
{
    public function getCompanyName(){

        if ($this->companyCard)
            return $this->companyCard->name;
    }

    public function getCompanyCard (){
        return $this->hasOne(CompanyCard::class,['id' => 'company_id']);
    }
}