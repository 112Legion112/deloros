<?php
namespace common\models\extended;

use common\models\Members as BaseMembers;
/**
 * @property CompanyCard $company;
 */

class Members extends BaseMembers
{
    public function getCompanyName(){
        if ($this->company)
            return $this->company->name;
    }

    public function getAvatarUrl ($size){
        $images = $this->getImages()[0];
        $imageUrl = $images->getUrl($size);
        return $imageUrl;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany(){
        return $this->hasOne(CompanyCard::class,['id' => 'company_card_id']);
    }
}