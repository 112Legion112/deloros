<?php
namespace common\models\extended;

use common\models\Partners as BasePartners;

class Partners extends BasePartners{
    public function getLogo($size){
        $image =  $this->getImages()[0];
        return $image->getUrl($size);
    }
}