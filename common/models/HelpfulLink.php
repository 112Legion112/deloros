<?php

namespace common\models;

use Yii;
use dvizh\gallery\models\Image;

/**
 * This is the model class for table "helpful_link".
 *
 * @property int $id
 * @property string $url
 * @property string $name
 * @method Image[] getImages
 */
class HelpfulLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'helpful_link';
    }


    public function behaviors()
    {
        return [
            'images' => [
                'class' => 'dvizh\gallery\behaviors\AttachImages',
                'mode' => 'single',
                'quality' => 60,
                'galleryId' => 'picture'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'name'], 'string', 'max' => 255],
            [['url'],'url'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}
