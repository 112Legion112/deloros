<?php

namespace common\models;

use Yii;
use dvizh\gallery\models\Image;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $text
 *
 * @method Image[] getImages();
 */
class Slider extends \yii\db\ActiveRecord
{

    function behaviors()
    {
        return [
            'images' => [
                'class' => 'dvizh\gallery\behaviors\AttachImages',
                'mode' => 'gallery',
                'quality' => 60,
                'galleryId' => 'picture'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 75],
            [['text'], 'string'],
            [['url'], 'string', 'max' => 255],
            [['url'], 'url'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'text' => Yii::t('app', 'Text'),
        ];
    }
}
