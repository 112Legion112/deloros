<?php

namespace common\models;

use dvizh\gallery\models\Image;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use dvizh\seo\models\Seo;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $name
 * @property string $anons
 * @property string $text
 * @property string $date
 * @property string $status
 * @property string $slug
 * @property Seo    $seo
 * @property int    $company_card_id
 *
 * @method Image[]  getImages()
 *
 */
class News extends \yii\db\ActiveRecord
{
    public static function getStatusPrompt(){
        return Yii::t('app', 'Status');
    }

    const STATUS_DRAFT = 1;
    const STATUS_PUBLISHED = 2;

    public static function getStatusOptions(){
        return [
            static::STATUS_DRAFT => Yii::t('app','Draft'),
            static::STATUS_PUBLISHED => Yii::t('app', 'Published'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return [
            'seo' => [
                'class' => 'dvizh\seo\behaviors\SeoFields',
            ],
            'images' => [
                'class' => 'dvizh\gallery\behaviors\AttachImages',
                'mode' => 'gallery',
                'quality' => 60,
                'galleryId' => 'picture'
            ],
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                 'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'anons', 'text'], 'required'],
            [['date'], 'safe'],
            [['status'], 'string'],
            [['status'], 'default', 'value' => self::STATUS_DRAFT],
            [['name','slug'], 'string', 'max' => 55],
            [['anons'], 'string', 'max' => 300],
            ['company_card_id','integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'News title'),
            'anons' => Yii::t('app', 'Anons'),
            'text' => Yii::t('app', 'Text'),
            'date' => Yii::t('app', 'Date'),
            'status' => Yii::t('app', 'Status'),
            'slug' => Yii::t('app', 'Slug'),
            'company_card_id' => Yii::t('app', 'Company card ID'),
        ];
    }
}
