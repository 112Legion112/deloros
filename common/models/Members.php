<?php

namespace common\models;

use Yii;
use \dvizh\gallery\models\Image;

/**
 * This is the model class for table "members".
 *
 * @property int $id
 * @property string $category
 * @property string $last_name
 * @property string $first_name
 * @property string $patronymic
 * @property string $text
 * @property string $title
 * @property int $company_card_id
 *
 * @method Image[] getImages()
 */
class Members extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'members';
    }

    function behaviors()
    {
        return [
            'images' => [
                'class' => 'dvizh\gallery\behaviors\AttachImages',
                'mode' => 'single',
                'quality' => 60,
                'galleryId' => 'picture',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'last_name', 'first_name','company_card_id'], 'required'],
            [['company_card_id'], 'integer'],
            [['category', 'last_name', 'first_name', 'patronymic'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 300],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'last_name' => Yii::t('app', 'Last Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'patronymic' => Yii::t('app', 'Patronymic'),
            'text' => Yii::t('app', 'Text'),
            'title' => Yii::t('app', 'Title'),
            'company_card_id' => Yii::t('app', 'Company card ID'),
        ];
    }
}
