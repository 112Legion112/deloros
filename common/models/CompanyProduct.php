<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company_product".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 */
class CompanyProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company title'),
            'name' => Yii::t('app', 'Product title'),
        ];
    }
}
