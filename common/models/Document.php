<?php
namespace common\models;

use Yii;
use nemmo\attachments\behaviors\FileBehavior;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property string $name
 * @property int $is_show
 */
class Document extends \yii\db\ActiveRecord
{
    const SHOW_IS_NO = 0;
    const SHOW_IS_YES = 1;
    public static function getShowOptions(){
        return [
            self::SHOW_IS_NO => Yii::t('app','No'),
            self::SHOW_IS_YES => Yii::t('app', 'Yes'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

    public function behaviors()
    {
        return [
		    'fileBehavior' => [
            'class' => FileBehavior::class
            ],
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
	    ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['is_show'], 'integer'],
            [['slug'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'is_show' => Yii::t('app', 'Is show'),
        ];
    }
}
