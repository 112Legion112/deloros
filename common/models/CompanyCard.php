<?php

namespace common\models;

use Yii;
use dvizh\gallery\models\Image;

/**
 * This is the model class for table "company_card".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $phone
 * @property string $email
 * @property string $site
 * @property string $address
 *
 * @method Image[] getImages()
 */
class CompanyCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_card';
    }

    public function behaviors()
    {
        return [
            'images' => [
                'class' => 'dvizh\gallery\behaviors\AttachImages',
                'mode' => 'single',
                'quality' => 100,
                'galleryId' => 'picture'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['description'], 'string'],
            [['name', 'phone', 'email', 'site', 'address'], 'string', 'max' => 255],
            [['email'],'email'],
            [['site'],'url'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'site' => Yii::t('app', 'Site'),
            'address' => Yii::t('app', 'Address'),
        ];
    }
}
