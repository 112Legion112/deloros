<?php
return [
    'language' => 'ru-Ru',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'settings' => [
            'class' => 'pheme\settings\components\Settings'
        ],
    ],
    'modules' => [
        'attachments' => [
            'class' => nemmo\attachments\Module::class,
            'tempPath' => '@frontend/uploads/temp',
            'storePath' => '@frontend/uploads/store',
            'rules' => [ // Rules according to the FileValidator
                'maxFiles' => 1, // Allow to upload maximum 3 files, default to 3
//                'mimeTypes' => 'image/png', // Only png images
                'maxSize' => 5120 * 5120 // 5 MB
            ],
//            'tableName' => '{{%attachments}}' // Optional, default to 'attach_file'
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => ['administrator'],
            // you will configure your module inside this file
            // or if need different configuration for frontend and backend you may
            // configure in needed configs
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'settings' => [
            'class' => 'pheme\settings\Module',
            'sourceLanguage' => 'en',
            'accessRoles' => ['admin'],
        ],
        'gallery' => [
            'class' => 'dvizh\gallery\Module',
            'imagesStorePath' => dirname(dirname(__DIR__)).'/frontend/web/images/store', //path to origin images
            'imagesCachePath' => dirname(dirname(__DIR__)).'/frontend/web/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD',
            'placeHolderPath' => '@webroot/images/placeHolder.png',
            'adminRoles' => ['admin'],
        ],
    ],
];