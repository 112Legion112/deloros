<?php

use yii\db\Migration;

/**
 * Handles adding url to table `slider`.
 */
class m180328_075500_add_url_column_to_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('slider', 'url', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('slider', 'url');
    }
}
