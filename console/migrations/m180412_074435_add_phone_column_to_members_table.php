<?php

use yii\db\Migration;

/**
 * Class m180412_064055_insert_phone_to_members_table
 */
class m180412_074435_add_phone_column_to_members_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('members', 'phone','string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('members', 'phone');
    }
}
