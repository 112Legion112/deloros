<?php

use yii\db\Migration;

/**
 * Class m180406_090909_delete_page_link_in_footer
 */
class m180406_090909_delete_page_link_in_footer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('page', [
            'title' => 'Сылки в футере',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
