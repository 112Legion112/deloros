<?php

use yii\db\Migration;

/**
 * Handles the creation of table `document`.
 */
class m180319_060555_create_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('document', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'is_show' => $this->integer(),
            'slug' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('document');
    }
}
