<?php

use yii\db\Migration;

/**
 * Handles adding text to table `slider`.
 */
class m180328_075606_add_text_column_to_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('slider', 'text', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('slider', 'text');
    }
}
