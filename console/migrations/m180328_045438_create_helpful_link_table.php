<?php

use yii\db\Migration;

/**
 * Handles the creation of table `helpful_link`.
 */
class m180328_045438_create_helpful_link_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('helpful_link', [
            'id' => $this->primaryKey(),
            'url' => $this->string(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('helpful_link');
    }
}
