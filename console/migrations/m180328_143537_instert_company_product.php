<?php

use yii\db\Migration;

/**
 * Class m180328_143537_instert_company_product
 */
class m180328_143537_instert_company_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('company_product', [
            'id' => 1,
            'company_id' => 2,
            'name' => 'Первый продукт',
        ]);
        $this->insert('company_product', [
            'id' => 2,
            'company_id' => 1,
            'name' => 'Второй продукт',
        ]);
        $this->insert('company_product', [
            'id' => 3,
            'company_id' => 1,
            'name' => 'Тритий продукт',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for($i=1; $i<=3; $i++){
            $this->delete('company_product', ['id' => $i]);
        }
    }

}
