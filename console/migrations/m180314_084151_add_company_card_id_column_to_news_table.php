<?php

use yii\db\Migration;

/**
 * Handles adding company_card_id to table `news`.
 */
class m180314_084151_add_company_card_id_column_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('news', 'company_card_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('news', 'company_card_id');
    }
}
