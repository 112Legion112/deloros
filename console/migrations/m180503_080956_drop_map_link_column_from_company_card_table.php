<?php

use yii\db\Migration;

/**
 * Handles dropping map_link from table `company_card`.
 */
class m180503_080956_drop_map_link_column_from_company_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('company_card', 'map_link');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('company_card', 'map_link', $this->string());
    }
}
