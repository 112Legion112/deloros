<?php

use yii\db\Migration;

/**
 * Handles adding address to table `company_card`.
 */
class m180503_081324_add_address_column_to_company_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company_card', 'address', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company_card', 'address');
    }
}
