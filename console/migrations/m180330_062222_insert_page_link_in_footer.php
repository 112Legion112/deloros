<?php

use yii\db\Migration;

/**
 * Class m180330_062222_insert_page_link_in_footer
 */
class m180330_062222_insert_page_link_in_footer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('page', [
            'title' => 'Сылки в футере',
            'text' => '<li><a href="">Программа</a></li><li><a href="">Условия</a></li><li><a href="#myModal" data-toggle="modal">Подать заявку</a></li>',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('page', [
            'title' => 'Сылки в футере',
        ]);
    }
}
