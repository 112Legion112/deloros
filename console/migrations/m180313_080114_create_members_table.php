<?php

use yii\db\Migration;

/**
 * Handles the creation of table `members`.
 */
class m180313_080114_create_members_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('members', [
            'id' => $this->primaryKey(),
            'category' => $this->integer()->notNull(),
            'last_name' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'patronymic' => $this->string(),
            'text' => $this->string(300),
            'title' => $this->string(50),
            'company_card_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('members');
    }
}
