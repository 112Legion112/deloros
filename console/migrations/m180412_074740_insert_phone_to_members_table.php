<?php

use yii\db\Migration;

/**
 * Class m180412_074740_insert_phone_to_members_table
 */
class m180412_074740_insert_phone_to_members_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for($i=1; $i<=12; $i++)
            $this->update('members',
                ['phone' => '+7 999 999 99 99'],
                ['id' => $i]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for($i=1; $i<=12; $i++)
            $this->update('members',
                ['phone' => null],
                ['id' => $i]
            );
    }
}
