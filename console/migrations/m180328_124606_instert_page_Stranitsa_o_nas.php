<?php

use yii\db\Migration;

/**
 * Class m180328_124606_instert_page_Stranitsa_o_nas
 */
class m180328_124606_instert_page_Stranitsa_o_nas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('page', [
            'title' => 'Страница о нас',
            'text'  => '<h1 class="text-center">Страница о нас</h1><h3 class="text-center">Контактный номер : 8 800 555 35 35</h3><h3 class="text-center">Время работы : пн-пт 11.30 - 18.00  сб-вс Выходной</h3><h2>
	</h2><h3 class="text-center">Адрес : Ул.Белозерского Д12а</h3><h2>
	</h2><h3 class="text-center">Мы на карте</h3><p class="text-center">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d4525.770320420034!2d65.36437205051878!3d55.44723432760175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sru!4v1522228932831" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
	</p>',
            'slug' => 'stranitsa-o-nas',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('page', ['slug' => 'stranitsa-o-nas']);
    }
}
