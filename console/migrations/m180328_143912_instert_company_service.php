<?php

use yii\db\Migration;

/**
 * Class m180328_143912_instert_company_service
 */
class m180328_143912_instert_company_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for ($i = 1; $i <= 7; $i++) {
            $this->insert('company_service', [
                'id' => $i,
                'company_id' => $i > 4 ? 1 : 2,
                'name' => 'Cервис ' . $i,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 1; $i <= 7; $i++) {
            $this->delete('company_service', [
                'id' => $i,
            ]);
        }
    }
}
