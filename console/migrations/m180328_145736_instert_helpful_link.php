<?php

use yii\db\Migration;

/**
 * Class m180328_145736_instert_helpful_link
 */
class m180328_145736_instert_helpful_link extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for($i=1; $i<=5; $i++) {
            $this->insert('helpful_link', [
                'id' => $i,
                'url' => 'http://example.com',
                'name' => 'Ссылка номер ' . $i,
            ]);
            $this->insert('image', [
                'filePath' => 'HelpfulLInks/HelpfulLInk1/3a307b.jpg',
                'itemId' => $i,
                'modelName' => 'HelpfulLink',
                'urlAlias' => '1312f8b940-2',
                'gallery_id' => 'picture',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for($i=1; $i<=5; $i++) {
            $this->delete('helpful_link', [
                'id' => $i,
            ]);
            $this->delete('image', [
                'itemId' => $i,
                'modelName' => 'HelpfulLink',
            ]);
        }
    }
}
