<?php

use yii\db\Migration;

/**
 * Class m180328_144949_instert_partners
 */
class m180328_144949_instert_partners extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for ($i=1; $i<=5; $i++) {
            $this->insert('partners', [
                'id' => $i,
                'name' => "Партнер - " . $i,
                'url' => 'http://example.com',
            ]);
        }

        $this->insert('image', [
            'filePath' => 'Partnerss/Partners1/8c45c3.png',
            'itemId' => 1,
            'modelName' => 'Partners',
            'urlAlias' => '6d049dd473-2',
            'gallery_id' => 'picture',
        ]);
        $this->insert('image', [
            'filePath' => 'Partnerss/Partners2/d42772.png',
            'itemId' => 2,
            'modelName' => 'Partners',
            'urlAlias' => 'eb1f9b0832-2',
            'gallery_id' => 'picture',
        ]);
        $this->insert('image', [
            'filePath' => 'Partnerss/Partners3/f91f15.png',
            'itemId' => 3,
            'modelName' => 'Partners',
            'urlAlias' => '86e84e12a9-2',
            'gallery_id' => 'picture',
        ]);
        $this->insert('image', [
            'filePath' => 'Partnerss/Partners4/579969.jpg',
            'itemId' => 4,
            'modelName' => 'Partners',
            'urlAlias' => '481df31280-2',
            'gallery_id' => 'picture',
        ]);
        $this->insert('image', [
            'filePath' => 'Partnerss/Partners5/edff80.jpg',
            'itemId' => 5,
            'modelName' => 'Partners',
            'urlAlias' => 'Partners',
            'gallery_id' => 'picture',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i=1; $i<=5; $i++) {
            $this->delete('partners', [
                'id' => $i,
            ]);
            $this->delete('image', [
                'itemId' => $i,
                'modelName' => 'Partners',
            ]);
        }
    }
}
