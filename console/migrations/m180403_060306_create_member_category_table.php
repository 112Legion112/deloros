<?php

use yii\db\Migration;

/**
 * Handles the creation of table `member_category`.
 */
class m180403_060306_create_member_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('member_category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);

        $this->insert('member_category',[
            'id' => 1,
            'title' => 'Совет',
        ]);
        $this->insert('member_category',[
            'id' => 2,
            'title' => 'Дирекция',
        ]);
        $this->insert('member_category',[
            'id' => 3,
            'title' => 'Члены деловой россии',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('member_category');
    }
}
