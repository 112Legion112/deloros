<?php

use yii\db\Migration;

/**
 * Class m180510_091306_update_settings_email
 */
class m180510_091306_update_settings_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update(
            'settings',
            ['value'=> 'deloros45@inbox.ru'],
            ['section' => 'site', 'key' => 'email']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180510_091306_update_settings_email cannot be reverted.\n";

        return false;
    }
}
