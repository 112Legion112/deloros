<?php

use yii\db\Migration;

class m170809_074059_insert_user_administrator  extends Migration{

    public function up(){
        $this->insert('user',[
            'username'          => 'administrator',
            'email'             => 'admin@deloros.dvizh.net',
            'password_hash'     => '$2y$10$k1LOVy3CYh44S7VjFB9vP.DWHI1ByNADoZvzGQaN35VnFK4Gy3xfi',
            'auth_key'          => 'BncGrB9YR3DWerJ9byKMJ7GK1MBddlcj',
            'confirmed_at'      => '1521092318',
            'created_at'        => '1521092318',
            'updated_at'        => '1521092318',
            'flags'             => '0',
            'last_login_at'     => '1521789310',
        ]);

    }

    public function down(){
        $this->delete('user', ['username' => 'administrator']);
    }
}