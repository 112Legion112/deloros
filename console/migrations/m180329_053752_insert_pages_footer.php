<?php

use yii\db\Migration;

/**
 * Class m180329_053752_insert_pages_footer
 */
class m180329_053752_insert_pages_footer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('page', [
            'title' => 'Подвал левая колонка',
            'text' => '<div class="footer-title"> О ФОНДЕ </div> <p>Региональный фонд развития промышленности реализует программу "Совместные займы" по финансированию проектов развития промышленных предприятий.</p>',
        ]);

        $this->insert('page', [
            'title' => 'Подвал контактная информация',
            'text' => 'Для получения подробной информации по освященным на сайте вопросам вы можете обратиться:'
        ]);

        $this->insert('page', [
            'title' => 'Подвал копирайт',
            'text' => 'Региональный фонд развития промышленности'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('page', [
            'title' => 'Подвал левая колонка',
        ]);
        $this->delete('page', [
            'title' => 'Подвал контактная информация',
        ]);
        $this->delete('page', [
            'title' => 'Подвал копирайт',
        ]);
    }
}
