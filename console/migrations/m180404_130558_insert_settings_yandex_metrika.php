<?php

use yii\db\Migration;

/**
 * Class m180404_130558_insert_settings_yandex_metrika
 */
class m180404_130558_insert_settings_yandex_metrika extends Migration
{
    public function safeUp()
    {
        $this->insert('settings',[
            'type' => 'string',
            'section' => 'yandex',
            'key' => 'metrika',
            'active' => 1,
            'created' => '2018-03-30 11:44:56',
            'modified' => '2018-03-30 11:47:06',
            'value' => '<!-- Yandex.Metrika counter --><script type="text/javascript" >    (function (d, w, c) {        (w[c] = w[c] || []).push(function() {            try {                w.yaCounter48242888 = new Ya.Metrika({                    id:48242888,                    clickmap:true,                    trackLinks:true,                    accurateTrackBounce:true                });            } catch(e) { }        });        var n = d.getElementsByTagName("script")[0],            s = d.createElement("script"),            f = function () { n.parentNode.insertBefore(s, n); };        s.type = "text/javascript";        s.async = true;        s.src = "https://mc.yandex.ru/metrika/watch.js";        if (w.opera == "[object Opera]") {            d.addEventListener("DOMContentLoaded", f, false);        } else { f(); }    })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/48242888" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings',[
            'section' => 'yandex',
            'key' => 'metrika',
        ]);
    }
}
