<?php

use yii\db\Migration;

/**
 * Class m180327_063935_page_insert
 */
class m180327_063935_page_insert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('page', [
            'title' => 'текст на главной-1',
            'text' => '<h1>Региональный фонд развития промышленности </h1>
<p>Фонд развития промышленности основан для модернизации Курганской промышленности, организации новых<br>производств и обеспечения импортозамещения.
</p>',
            'slug' => 'tekst-na-glavnoy-1'
        ]);
        $this->insert('page', [
            'title' => 'текст на главной-2',
            'text' => '<h3>Идейные соображения высшего порядка.</h3>
<p>Таким образом постоянный количественный рост и сфера нашей активности требуют от нас анализа дальнейших направлений развития. Повседневная практика показывает, что начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании новых предложений.
</p>
<p>Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании систем массового участия. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности играет важную роль в формировании модели развития.
</p>
<p>Разнообразный и богатый опыт рамки и место обучения кадров играет важную роль в формировании модели развития. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности позволяет выполнять важные задания по разработке систем массового участия.
</p><br><a href="#" class="bttn">Читать</a>',
            'slug' => 'tekst-na-glavnoy-2'
        ]);
        $this->insert('page', [
            'title' => 'текст на главной-партнеры',
            'text' => '<h2>Партнеры фонда</h2>
<p>Фонд развития промышленности основан для модернизации Курганской промышленности,<br>организации новых производств и обеспечения импортозамещения.</p>',
            'slug' => 'tekst-na-glavnoy-partnery'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('news', ['slug' => 'tekst-na-glavnoy-1']);
        $this->delete('news', ['slug' => 'tekst-na-glavnoy-2']);
        $this->delete('news', ['slug' => 'tekst-na-glavnoy-partnery']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180327_063935_page_insert cannot be reverted.\n";

        return false;
    }
    */
}
