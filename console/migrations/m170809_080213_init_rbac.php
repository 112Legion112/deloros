<?php

use dektrium\user\models\User;
use yii\db\Migration;

class m170809_080213_init_rbac  extends  Migration{

    public function up()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $userAdministrator = User::find()->where(['username'=>'administrator'])->one()->id;
        $auth->assign($admin, $userAdministrator);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }
}