<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partners`.
 */
class m180311_133830_create_partners_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('partners', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'url' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('partners');
    }
}
