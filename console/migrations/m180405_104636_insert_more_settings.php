<?php

use yii\db\Migration;

/**
 * Class m180405_104636_insert_more_settings
 */
class m180405_104636_insert_more_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('page', [
            'title' => 'Страница о нас',
        ]);
        $this->insert('settings', [
            'active' => 1,
            'created' => '2018-03-22 10:33:23',
            'type' => 'string',
            'section' => 'site',
            'key' => 'time',
            'value' => 'пн-пт 11.30 - 18.00 сб-вс Выходной',
        ]);
        $this->insert('settings', [
            'active' => 1,
            'created' => '2018-03-22 10:33:23',
            'type' => 'string',
            'section' => 'site',
            'key' => 'map',
            'value' => 'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d4525.770320420034!2d65.36437205051878!3d55.44723432760175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sru!4v1522228932831',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings', [
            'section' => 'site',
            'key' => 'time',
        ]);
        $this->delete('settings', [
            'section' => 'site',
            'key' => 'map',
        ]);
    }
}
