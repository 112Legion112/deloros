<?php

use yii\db\Migration;

/**
 * Class m180328_151209_instert_settings
 */
class m180328_151209_instert_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'id'=> 1,
            'section' => 'site',
            'type' => 'email',
            'key' => 'email',
            'value' => 'admin@site.com',
            'active' => 1,
            'created' => '2018-03-22 10:33:23'
        ]);
        $this->insert('settings', [
            'id'=> 2,
            'section' => 'site',
            'key' => 'address',
            'type' => 'string',
            'value' => 'г. Курган ул. Пробная дом 3',
            'active' => 1,
            'created' => '2018-03-22 10:33:23'
        ]);
        $this->insert('settings', [
            'id'=> 3,
            'section' => 'site',
            'type' => 'string',
            'key' => 'phone',
            'value' => '+7909-444-44-44',
            'active' => 1,
            'created' => '2018-03-22 10:33:23'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for($i=1; $i<=3; $i++){
            $this->delete('settings', [
                'id' => $i,
            ]);
        }
    }
}
