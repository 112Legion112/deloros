<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m180309_114213_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'name' => $this->string(55)->notNull(),
            'anons' => $this->string(300)->notNull(),
            'text' => $this->text()->notNull(),
            'date' => $this->date(),
            'status' => $this->integer()->notNull(),
            'slug' => $this->string(55),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
