<?php

use yii\db\Migration;

/**
 * Handles adding map_link to table `company_card`.
 */
class m180328_100242_add_map_link_column_to_company_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company_card', 'map_link', $this->string());
        $this->update('company_card',
            ['map_link' => 'https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d2263.3040658161003!2d65.3435323!3d55.4399299!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1522060791650'],
            'id=1'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company_card', 'map_link');
    }
}
