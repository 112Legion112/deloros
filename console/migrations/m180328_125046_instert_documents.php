<?php

use yii\db\Migration;

/**
 * Class m180328_125046_instert_documents
 */
class m180328_125046_instert_documents extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('document', [
            'id' => 1,
            'name' => 'Название текстового документа',
            'is_show' => '1',
            'slug' => 'nazvaniye-tekstovogo-dokumenta'
        ]);

        $this->insert('attach_file', [
            'name' => 'ustav_delovaya_rossiya',
            'model' => 'Document',
            'itemId' => 1,
            'hash' => '7bf198ee9d6d70714cf18263457c9f7b',
            'size' => '109056',
            'type' => 'doc',
            'mime' => 'application/msword',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('document', ['slug' => 'nazvaniye-tekstovogo-dokumenta']);

        $this->delete('attach_file', ['hash' => '7bf198ee9d6d70714cf18263457c9f7b']);
    }
}
