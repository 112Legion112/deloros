<?php

use yii\db\Migration;

/**
 * Class m180328_085628_insert_slider
 */
class m180328_085628_insert_slider extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('slider', [
            'id' => 1,
            'name' => 'Документы',
            'url' => './document/index',
            'text' => 'По этой ссылке вы можете перейти в наши документы',
        ]);
        $this->insert('slider', [
            'id' => 2,
            'name' => 'О нас',
            'url' => './site/about',
            'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        ]);
        $this->insert('slider', [
            'id' => 3,
            'name' => 'Члены',
            'url' => './members',
            'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        ]);

        $this->insert('image', [
            'filePath' => 'Sliders/Slider1/727cd9.jpg',
            'itemId' => 1,
            'modelName' => 'Slider',
            'urlAlias' => '8c6466cf56-3',
            'gallery_id' => 'picture',
        ]);
        $this->insert('image', [
            'filePath' => 'Sliders/Slider2/682630.jpg',
            'itemId' => 2,
            'modelName' => 'Slider',
            'urlAlias' => '0f5368782b-2',
            'gallery_id' => 'picture',
        ]);
        $this->insert('image', [
            'filePath' => 'Sliders/Slider3/80f143.jpg',
            'itemId' => 3,
            'modelName' => 'Slider',
            'urlAlias' => 'f5e6775400-2',
            'gallery_id' => 'picture',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for($i=1; $i<=3; $i++) {
            $this->delete('slider', [
                'id' => $i,
            ]);
            $this->delete('image', [
                'itemId' => $i,
                'modelName' => 'Slider',
            ]);
        }
    }


}
