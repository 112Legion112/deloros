<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_card`.
 */
class m180313_090355_create_company_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company_card', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'site' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company_card');
    }
}
